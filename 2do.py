# MATIAS FRANCISCO MAMANI
import os
def pulsarTecla():
    input('Pulse un tecla...')

def Registro():
    os.system('cls')
    codigo=1
    while codigo!=0:
        codigo=int(input('Ingrese Codigo (cero para finalizar): '))
        if codigo!=0:
            if codigo not in Productos:
                Desc=input('Descripcion del Producto: ')
                Precio=int(input('Ingrese el precio del Producto: '))
                while Precio<0:
                    print('El precio debe ser un valor Positivo')
                    Precio=int(input('Ingrese nuevamente el precio del Producto: '))
                Stock=int(input('Ingrese la cantidad de Stock: '))
                while Stock<0:
                    print('El Stock debe ser un valor Positivo')
                    Stock=int(input('Ingrese nuevamente la cantidad de Stock: '))
                Productos[codigo]=[Desc,Precio,Stock]
                os.system('cls')
            else:
                print('El codigo ya esta registrado. Debe introducir uno nuevo')
    return Productos

def Mostrar(Productos):
    os.system('cls')
    print('Lista de Productos:')
    for codigo,producto in Productos.items():
        print('Codigo:',codigo,' Descripcion:',producto[0],' Precio:',producto[1],' Stock:',producto[2])


def MostrarRango(Productos):
    os.system('cls')
    cont=0
    cont2=len(Productos)
    A=int(input('Minimo valor del intervalo: '))
    B=int(input('Maximo valor del intervalo: '))
    while B<A:
        print('El valor Minimo debe ser menor al valor Maximo')
        A=int(input('Minimo valor del intervalo: '))
        B=int(input('Maximo valor del intervalo: '))
    print('')
    print(f'Lista de productos dentro del rango de Stock {A} hasta {B}: ')
    for codigo in Productos:
        if Productos[codigo][2]>= A and Productos[codigo][2]<=B:
            print(codigo,Productos[codigo])
        else:
            cont+=1
    if cont==cont2:
        print('No se encontro ningun Stock')

def Suma(Productos):
    os.system('cls')
    X=int(input('Ingrese el valor de X: '))
    Y=int(input('Ingrese el valor de Y: '))
    cont=0
    cont2=len(Productos)
    print(f'Productos a los cuales se le a sumado {X} :')
    for codigo in Productos:
        if Productos[codigo][2]<Y:
            Productos[codigo][2]+=X
            print(codigo,Productos[codigo])
        else:
            cont+=1
    if cont==cont2:
        print('No se encontro ningun Stock')

def verificar(Productos):
    pos=-1
    for codigo,valor in Productos.items():
        if valor[2] == 0:
            pos= codigo
            break
    return pos

def eliminar(Productos):
    os.system 
    pos=1
    while pos!=-1:
        pos=verificar(Productos)
        if pos==-1:
            Mostrar(Productos)
        else:
            del Productos[pos] 
    return Productos       
    
def Menu():
    os.system('cls')
    print('     ')
    print('1- Registrar Productos:')
    print('2- Mostrar Productos:')
    print('3- Mostrar Productos con cuyo Stock de intervalos:')
    print('4- Sume X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y:')
    print('5- Eliminar todos los productos cuyo stock sea igual a cero:')
    print('6- Salir')
    opcion = int(input('Seleccione Una Opcion :'))

    while not (opcion >= 1 and opcion <= 6):
        opcion = int(input('Seleccione una Opcion :')) 
    return opcion

#principal
Productos={}
opcion= Menu()
while opcion!= 6:

    if opcion == 1:
        Productos=Registro()
    elif opcion == 2:
        Mostrar(Productos)
        pulsarTecla()
    elif opcion == 3:
        MostrarRango(Productos)
        pulsarTecla()
    elif opcion == 4:
        Suma(Productos)
        pulsarTecla()
    elif opcion == 5:
        eliminar(Productos)
        pulsarTecla()
    elif opcion == 6:
        print('Fin del Programa')
    opcion=Menu()